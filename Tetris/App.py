'''
Created on 08.02.2012

@author: marce.at
'''
from model.engine import Engine
import sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QMainWindow
from gui.BaseWindow import Ui_MainWindow as MainWindow

if __name__ == '__main__':
    engine = Engine(300,660,30)   
    app = QtGui.QApplication(sys.argv)    
    
    gui = QMainWindow()
    
    #gui = QtGui.QWidget()
    
    panel = MainWindow()
    panel.setupUi(gui)
    panel.setEngine(engine)
    
    #panel = MainWindow.Ui_PlainWinampWindow() #das is deine erzeugte klasse
    #panel.setupUi(gui)    
   
    gui.show()
    app.exec_()
    
    