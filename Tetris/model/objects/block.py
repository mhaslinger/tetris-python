'''
Created on 08.02.2012

@author: marce.at
'''
from collections import namedtuple

class Block:
    '''
    one of the four blocks of each figure
    '''

    def __init__(self, x, y, size):
        self._x1 = x
        self._y1 = y
        self._x2 = self._x1 + size
        self._y2 = self._y1 - size
        self._size = size
        self.color = 0

    def shiftDown(self):
        self._y1 += self._size
        self._y2 = self._y1 - self._size

    def shiftLeft(self):
        self._x2 = self._x1
        self._x1 -= self._size

    def shiftRight(self):
        self._x1 = self._x2
        self._x2 += self._size

    def shiftUp(self):
        self._y1 -= self._size
        self._y2 = self._y1 + self._size

    def setPos(self, newX, newY):
        self._x1 = newX
        self._y1 = newY
        self._x2 = self._x1 + self._size
        self._y2 = self._y1 - self._size

    def getFieldPos(self):
        Point = namedtuple('Point', 'x y')
        p = Point(self._x1 // self._size, self._y1 // self._size)
        return p

    def isInColFrame(self, colframe):
        if (self._x1 >= colframe.x1) and (self._x2 <= colframe.x2):
            if (self._y1 <= colframe.y1) and (self._y2 >= colframe.y2):
                return True
        return False

        
