'''
Created on 08.02.2012

@author: marce.at
'''
from .block import Block
import random

class _Figure:
    '''
    abstract base class of all figures
    '''

    __slots__ = ["blocksize", "color", "blocks"]

    def __init__(self, blocksize):
        self.blocksize = blocksize

    def doShiftDown(self):
        for b in self.blocks:
            b.shiftDown()
        self.colFrame.down()

    def doShiftLeft(self):
        for b in self.blocks:
            b.shiftLeft()
        self.colFrame.left()

    def doShiftRight(self):
        for b in self.blocks:
            b.shiftRight()
        self.colFrame.right()

    def isFamily(self, block):
        #return any(block == b for b in self.blocks)
        for b in self.blocks:
            if block == b:
                return True
        return False

    def getSortedBlocksD(self):
        self.blocks.sort(key=lambda block: block._y1, reverse=True)
        return self.blocks

    def getSortedBlocksL(self):
        self.blocks.sort(key=lambda block: block._x1, reverse=False)
        return self.blocks

    def getSortedBlocksR(self):
        self.blocks.sort(key=lambda block: block._x2, reverse=True)
        return self.blocks

    def doRotate(self, blocks):
        if self.colFrame.checkCollision(blocks):
            self.rotate()

    def rotate(self):
        pass

    def colorize(self):
        i = random.randint(1,6)
        self.color = i
        for b in self.blocks:
            b.color = self.color

class ColFrame:
    def __init__(self, x, y, blocksize,line):
        self.x1 = x-blocksize
        if line:
            self.x2 = self.x1 + 4 * blocksize
            self.y1 = y
            self.y2 = y - 4 * blocksize
        else:
            self.x2 = self.x1 + 3 * blocksize
            self.y1 = y-blocksize
            self.y2 = y - 3 * blocksize
        self.blocksize = blocksize

    def down(self):
        self.y1 += self.blocksize
        self.y2 += self.blocksize

    def left(self):
        self.x1 -= self.blocksize
        self.x2 -= self.blocksize

    def right(self):
        self.x1 += self.blocksize
        self.x2 += self.blocksize

    def checkCollision(self, blocks):
        for b in blocks:
            if b.isInColFrame(self):
                return False
        return True


class Square(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)

    def setUp(self, x1, y1):
        b1 = Block(x1 - self.blocksize, y1, self.blocksize);
        b2 = Block(x1, y1, self.blocksize);
        b3 = Block(x1 - self.blocksize, y1 - self.blocksize, self.blocksize)
        b4 = Block(x1, y1 - self.blocksize, self.blocksize);
        self.blocks = [b1, b2, b3, b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize,False)

    def doRotate(self, blocks):
        pass


class Line(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1, y1 - self.blocksize, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1, y1 + self.blocksize, self.blocksize)
        self.b4 = Block(x1, y1 + 2 * self.blocksize, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize,True)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftLeft()
            self.b1.shiftDown()
            self.b3.shiftRight()
            self.b3.shiftUp()
            self.b4.shiftRight()
            self.b4.shiftRight()
            self.b4.shiftUp()
            self.b4.shiftUp()
            self.rotState = 1
        else:
            self.b1.shiftUp()
            self.b1.shiftRight()
            self.b3.shiftDown()
            self.b3.shiftLeft()
            self.b4.shiftDown()
            self.b4.shiftDown()
            self.b4.shiftLeft()
            self.b4.shiftLeft()
            self.rotState = 0


class L(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1, y1 - self.blocksize, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1, y1 + self.blocksize, self.blocksize)
        self.b4 = Block(x1 + self.blocksize, y1 + self.blocksize, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize,False)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftRight()
            self.b1.shiftDown()
            self.b3.shiftLeft()
            self.b3.shiftUp()
            self.b4.shiftLeft()
            self.b4.shiftLeft()
            self.rotState = 1
        elif self.rotState == 1:
            self.b1.shiftDown()
            self.b1.shiftLeft()
            self.b3.shiftUp()
            self.b3.shiftRight()
            self.b4.shiftUp()
            self.b4.shiftUp()
            self.rotState = 2
        elif self.rotState == 2:
            self.b1.shiftLeft()
            self.b1.shiftUp()
            self.b3.shiftRight()
            self.b3.shiftDown()
            self.b4.shiftRight()
            self.b4.shiftRight()
            self.rotState = 3
        elif self.rotState == 3:
            self.b1.shiftUp()
            self.b1.shiftRight()
            self.b3.shiftDown()
            self.b3.shiftLeft()
            self.b4.shiftDown()
            self.b4.shiftDown()
            self.rotState = 0

class J(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1, y1 - self.blocksize, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1, y1 + self.blocksize, self.blocksize)
        self.b4 = Block(x1 - self.blocksize, y1 + self.blocksize, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize, False)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftRight()
            self.b1.shiftDown()
            self.b3.shiftLeft()
            self.b3.shiftUp()
            self.b4.shiftUp()
            self.b4.shiftUp()
            self.rotState = 1
        elif self.rotState == 1:
            self.b1.shiftDown()
            self.b1.shiftLeft()
            self.b3.shiftUp()
            self.b3.shiftRight()
            self.b4.shiftRight()
            self.b4.shiftRight()
            self.rotState = 2
        elif self.rotState == 2:
            self.b1.shiftLeft()
            self.b1.shiftUp()
            self.b3.shiftRight()
            self.b3.shiftDown()
            self.b4.shiftDown()
            self.b4.shiftDown()
            self.rotState = 3
        elif self.rotState == 3:
            self.b1.shiftUp()
            self.b1.shiftRight()
            self.b3.shiftDown()
            self.b3.shiftLeft()
            self.b4.shiftLeft()
            self.b4.shiftLeft()
            self.rotState = 0

class S(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1-self.blocksize, y1, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1, y1 - self.blocksize, self.blocksize)
        self.b4 = Block(x1 + self.blocksize, y1 - self.blocksize, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize, False)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftUp()
            self.b1.shiftRight()
            self.b3.shiftRight()
            self.b3.shiftDown()
            self.b4.shiftDown()
            self.b4.shiftDown()
            self.rotState = 1
        else:
            self.b1.shiftDown()
            self.b1.shiftLeft()
            self.b3.shiftUp()
            self.b3.shiftLeft()
            self.b4.shiftUp()
            self.b4.shiftUp()
            self.rotState = 0

class Z(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1-self.blocksize, y1-self.blocksize, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1, y1 - self.blocksize, self.blocksize)
        self.b4 = Block(x1 + self.blocksize, y1, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize, False)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftRight()
            self.b1.shiftRight()
            self.b3.shiftRight()
            self.b3.shiftDown()
            self.b4.shiftDown()
            self.b4.shiftLeft()
            self.rotState = 1
        else:
            self.b1.shiftLeft()
            self.b1.shiftLeft()
            self.b3.shiftUp()
            self.b3.shiftLeft()
            self.b4.shiftRight()
            self.b4.shiftUp()
            self.rotState = 0

class T(_Figure):
    def __init__(self, centerPosX, centerPosY, blocksize):
        super().__init__(blocksize)
        self.setUp(centerPosX, centerPosY)
        self.rotState = 0

    def setUp(self, x1, y1):
        self.b1 = Block(x1 - self.blocksize, y1, self.blocksize);
        self.b2 = Block(x1, y1, self.blocksize);
        self.b3 = Block(x1 + self.blocksize, y1, self.blocksize)
        self.b4 = Block(x1, y1 + self.blocksize, self.blocksize);
        self.blocks = [self.b1, self.b2, self.b3, self.b4]
        self.colFrame = ColFrame(x1 - self.blocksize, y1 + 2 * self.blocksize, self.blocksize, False)

    def rotate(self):
        if self.rotState == 0:
            self.b1.shiftRight()
            self.b1.shiftUp()
            self.b3.shiftLeft()
            self.b3.shiftDown()
            self.b4.shiftLeft()
            self.b4.shiftUp()
            self.rotState = 1
        elif self.rotState == 1:
            self.b1.shiftDown()
            self.b1.shiftRight()
            self.b3.shiftUp()
            self.b3.shiftLeft()
            self.b4.shiftRight()
            self.b4.shiftUp()
            self.rotState = 2
        elif self.rotState == 2:
            self.b1.shiftLeft()
            self.b1.shiftDown()
            self.b3.shiftRight()
            self.b3.shiftUp()
            self.b4.shiftRight()
            self.b4.shiftDown()
            self.rotState = 3
        elif self.rotState == 3:
            self.b1.shiftUp()
            self.b1.shiftLeft()
            self.b3.shiftDown()
            self.b3.shiftRight()
            self.b4.shiftDown()
            self.b4.shiftLeft()
            self.rotState = 0

class FigureFactory():
    def __init__(self):
        pass

    def getFigure(self, centerPosX, centerPosY, blocksize):
        i = random.randint(1,7)
        if i == 1:
            return Square(centerPosX, centerPosY, blocksize)
        elif i == 2:
            return Line(centerPosX, centerPosY, blocksize)
        elif i == 3:
            return L(centerPosX, centerPosY, blocksize)
        elif i == 4:
            return J(centerPosX, centerPosY, blocksize)
        elif i == 5:
            return S(centerPosX, centerPosY, blocksize)
        elif i == 6:
            return S(centerPosX, centerPosY, blocksize)
        else:
            return T(centerPosX, centerPosY, blocksize)
        
    
        
        