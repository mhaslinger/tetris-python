'''
Created on 08.02.2012

@author: marce.at
'''
from .objects.figures import Line

from .objects.figures import FigureFactory
from PyQt4 import QtGui
from threading import Timer
import math

class Engine:
    '''
    classdocs
    '''

    def __init__(self, width, height, blocksize):
        self.field = _Playground(width, height, blocksize)
        self.bs = blocksize
        self.factory = FigureFactory()
        self.activeFigure = self._genFig()
        self.preparedFigure = self._genFig()
        self._setFigure()
        self.points = 0
        self.level = 1
        timer = Timer(1.0, self._timerDown)
        timer.start()

    def down(self):
        if self.field.checkDownShift(self.activeFigure):
            self.field.doDownShift(self.activeFigure)
            self.pw.update()
        else:
            self._trashFig(self.activeFigure)
            self._clearLines()
        #self._debugPrint()

    def left(self):
        if self.field.checkLeftShift(self.activeFigure):
            self.field.doLeftShift(self.activeFigure)
            self.pw.update()
            self.pw.repaint()
            self._debugPrint()

    def right(self):
        if self.field.checkRightShift(self.activeFigure):
            self.field.doRightShift(self.activeFigure)
            self.pw.repaint()
            self._debugPrint()

    def rotate(self):
            self.field.doRotate(self.activeFigure)
            self.pw.repaint()
            self._debugPrint()

    def _trashFig(self, figure):
        self.activeFigure = self.preparedFigure
        self.preparedFigure = self._genFig()
        self._setFigure()

    def _genFig(self):
        return self.factory.getFigure(self.field.width // 2, 0 + 2 * self.bs, self.bs)

    def doFullPaint(self, qp):
        for block in self.preparedFigure.blocks:
            color = QtGui.QColor(0, 0, 0)
            qp.setPen(color)
            qp.setBrush(self._getColor(block.color))
            qp.drawRect(block._x1 - block._size + 100, block._y1 + 650, block._size, block._size)
        for block in self.field.getBlocks():
            self._doBlockPaint(qp, block)

    def _doBlockPaint(self, qp, b):
        color = QtGui.QColor(0, 0, 0)
        qp.setPen(color)
        qp.setBrush(self._getColor(b.color))
        qp.drawRect(b._x1 - b._size, b._y1, b._size, b._size)

    def _getColor(self, cid):
        if cid == 0:
            return QtGui.QColor(99,99,99)
        elif cid == 1:
            return QtGui.QColor(200, 0, 0)
        elif cid == 2:
            return QtGui.QColor(65,105,225)
        elif cid == 3:
            return QtGui.QColor(255,140,0)
        elif cid == 4:
            return QtGui.QColor(0,205,0)
        elif cid == 5:
            return QtGui.QColor(160,32,240)
        else:
            return QtGui.QColor(255,255,0)


    def _setFigure(self):
        self.activeFigure.colorize()
        self.field.addFigure(self.activeFigure)

    def _timerDown(self):
        self.down()
        self.mw.Points_La.setText(str(math.ceil(self.points)))
        self.mw.Level_La.setText(str(self.level))
        timer = Timer(self._calcInterval(), self._timerDown)
        timer.start()

    def _calcInterval(self):
        i = self.points
        if i < 100:
            self.level = 1
            return 1.0
        elif i < 200:
            self.level = 2
            return 0.9
        elif i < 400:
            self.level = 3
            return 0.8
        elif i < 600:
            self.level = 4
            return 0.7
        elif i < 800:
            self.level = 5
            return 0.6
        elif i < 1000:
            self.level = 6
            return 0.5
        elif i < 1500:
            self.level = 7
            return 0.4
        elif i < 2000:
            self.level = 8
            return 0.3
        elif i < 3000:
            self.level = 9
            return 0.2
        elif i < 5000:
            self.level = 10
            return 0.1
        else:
            self.level = 11
            return 0.05


    def _clearLines(self):
        self.points += self.field._clearLines() * (self.level*0.5)

    def setPaintWidget(self, pw):
        self.pw = pw

    def _debugPrint(self):
        self.field._debugPrint()

    def setMainWindow(self, mw):
        self.mw = mw


class _Playground:
    def __init__(self, width, height, bs):
        self.field = {}
        for i in range(1, (height // bs) + 1):
            self.field[i] = [None] * (width // bs)
        self.width = width
        self.height = height
        self.bs = bs

    def checkDownShift(self, figure):
        for b in figure.blocks:
            if not self._cbd(b, figure):
                return False
        return True

    def checkLeftShift(self, figure):
        for b in figure.blocks:
            if not self._cbl(b, figure):
                return False
        return True

    def checkRightShift(self, figure):
        for b in figure.blocks:
            if not self._cbr(b, figure):
                return False
        return True

    def doDownShift(self, figure):
        for b in figure.getSortedBlocksD():
            p = b.getFieldPos()
            self.field.get(p.y)[p.x - 1] = None
            self.field.get(p.y + 1)[p.x - 1] = b
        figure.doShiftDown()
        self.getBlocks()

    def doLeftShift(self, figure):
        for b in figure.getSortedBlocksL():
            p = b.getFieldPos()
            self.field.get(p.y)[p.x - 1] = None
            self.field.get(p.y)[p.x - 2] = b
        figure.doShiftLeft()

    def doRightShift(self, figure):
        for b in figure.getSortedBlocksR():
            p = b.getFieldPos()
            self.field.get(p.y)[p.x - 1] = None
            self.field.get(p.y)[p.x] = b
        figure.doShiftRight()

    def _cbd(self, block, figure):
        p = block.getFieldPos()
        if p.y == (self.height - block._size) // block._size:
            return False
        lst = self.field.get(p.y + 1)
        if lst[p.x - 1] is not None:
            if not figure.isFamily(lst[p.x - 1]):
                return False
        return True

    def _cbl(self, block, figure):
        p = block.getFieldPos()
        if p.x == 1:
            return False
        if self.field.get(p.y)[p.x - 2]:
            if not figure.isFamily(self.field.get(p.y)[p.x - 2]):
                return False;
        return True

    def _cbr(self, block, figure):
        p = block.getFieldPos()
        if p.x == self.width // self.bs:
            return False
        if self.field.get(p.y)[p.x]:
            if not figure.isFamily(self.field.get(p.y)[p.x]):
                return False;
        return True

    def addFigure(self, figure):
        self.activeFigure = None
        self.activeFigure = figure
        for block in figure.blocks:
            self._addBlock(block)

    def _addBlock(self, block):
        p = block.getFieldPos()
        self.field.get(p.y)[p.x - 1] = block

    def _clearLines(self):
        i = self.height // self.bs
        ret = 0
        cnt = 0
        while (i != 0):
            exitF = False
            for b in self.field.get(i):
                if b is None:
                    exitF = True
                    break
            if exitF:
                i -= 1
            else:
                tmp = self._remLine(i)
                if tmp > 0:
                    cnt += 1
                ret += tmp
                i = self.height // self.bs
        return ret * cnt

    def _remLine(self, i):
        self.field[i] = [None] * (self.width // self.bs)
        while (i != 0):
            for b in self.field.get(i):
                if not (b is None):
                    if not self.activeFigure.isFamily(b):
                        p = b.getFieldPos()
                        self.field.get(p.y)[p.x - 1] = None
                        self.field.get(p.y + 1)[p.x - 1] = b
                        b.shiftDown()
            i -= 1
        return self.width // self.bs

    def getBlocks(self):
        ret = []
        for i in range(1, (self.height // self.bs) + 1):
            lst = self.field[i]
            for b in lst:
                if b is not None:
                    ret.append(b)
        return ret

    def doRotate(self, figure):
        #x1 = figure.getSortedBlocksL()[0].getFieldPos().x
        #x2 = figure.getSortedBlocksR()[3].getFieldPos().x
        x1 = figure.colFrame.x1
        x2 = 0
        if isinstance(figure,Line):
            x2 = figure.colFrame.x1+3*self.bs
        else:
            x2 = figure.colFrame.x1+2*self.bs
        print('x1:'+str(x1)+' x2:'+str(x2)+' wid:'+str(self.width))
        if not (x1<0 or x2>(self.width-self.bs)):
            for b in figure.blocks:
                p = b.getFieldPos()
                self.field.get(p.y)[p.x - 1] = None
            lst = self.getBlocks()
            figure.doRotate(lst)
            self.addFigure(figure)

    def _debugPrint(self):
        print("")
        for line in self.field.values():
            s = ""
            for t in line:
                if t is None:
                    s += "-"
                else:
                    s += "x"
            print(s)