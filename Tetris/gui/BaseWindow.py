# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tet1.ui'
#
# Created: Wed Feb  8 22:26:11 2012
#      by: PyQt4 UI code generator 4.9
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from .paintWidget import PaintWidget

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(310, 830)
        MainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 680, 120, 80))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.label = QtGui.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(10, 20, 46, 13))
        self.label.setObjectName(_fromUtf8("label"))
        self.Points_La = QtGui.QLabel(self.groupBox)
        self.Points_La.setGeometry(QtCore.QRect(70, 20, 46, 13))
        self.Points_La.setObjectName(_fromUtf8("Points_La"))
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(10, 40, 46, 13))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(10, 60, 46, 13))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.Level_La = QtGui.QLabel(self.groupBox)
        self.Level_La.setGeometry(QtCore.QRect(70, 40, 46, 13))
        self.Level_La.setObjectName(_fromUtf8("Level_La"))
        self.Time_La = QtGui.QLabel(self.groupBox)
        self.Time_La.setGeometry(QtCore.QRect(70, 60, 46, 13))
        self.Time_La.setObjectName(_fromUtf8("Time_La"))
        
        self.paint_widget = PaintWidget(self.centralwidget)
        self.paint_widget.setGeometry(QtCore.QRect(0, 0, 302, 800))
        self.paint_widget.setObjectName(_fromUtf8("paint_widget")) 
        self.paint_widget.setFocus()     
        
        #self.paint_widget = QtGui.QWidget(self.centralwidget)
        #self.paint_widget.setGeometry(QtCore.QRect(0, 0, 302, 770))
        #self.paint_widget.setObjectName(_fromUtf8("paint_widget"))
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 310, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionNewGame = QtGui.QAction(MainWindow)
        self.actionNewGame.setObjectName(_fromUtf8("actionNewGame"))
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.menuFile.addAction(self.actionNewGame)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        
        #self.centralwidget.keyPressEvent()
        

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "Tetris", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Overview:", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Points:", None, QtGui.QApplication.UnicodeUTF8))
        self.Points_La.setText(QtGui.QApplication.translate("MainWindow", "0", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("MainWindow", "Level:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("MainWindow", "Time:", None, QtGui.QApplication.UnicodeUTF8))
        self.Level_La.setText(QtGui.QApplication.translate("MainWindow", "1", None, QtGui.QApplication.UnicodeUTF8))
        self.Time_La.setText(QtGui.QApplication.translate("MainWindow", "00:00", None, QtGui.QApplication.UnicodeUTF8))
        self.menuFile.setTitle(QtGui.QApplication.translate("MainWindow", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNewGame.setText(QtGui.QApplication.translate("MainWindow", "NewGame", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setText(QtGui.QApplication.translate("MainWindow", "Exit", None, QtGui.QApplication.UnicodeUTF8))
    
    def setEngine(self,engine):
        self.paint_widget.setEngine(engine)
        engine.setMainWindow(self)
        
    

