'''
Created on 08.02.2012

@author: marce.at
'''
from PyQt4 import QtGui,QtCore

class PaintWidget(QtGui.QWidget):
    '''
    classdocs
    '''

    def __init__(self,center):
        super().__init__(center)
        
    def paintEvent(self, e):
        self.clearMask()
        qp = QtGui.QPainter()
        qp.begin(self)        
        self.engine.doFullPaint(qp)
        qp.end()
        
    def setEngine(self,e):
        self.engine = e
        e.setPaintWidget(self)
        
    def keyPressEvent(self, e):        
        if e.key() == QtCore.Qt.Key_Left:
            self.engine.left()
            #print("left!")
        elif e.key() == QtCore.Qt.Key_Right:
            self.engine.right()
            #print("right!")
        elif e.key() == QtCore.Qt.Key_Down:
            self.engine.down()
            #print("down!")
        elif e.key() == QtCore.Qt.Key_Up:
            self.engine.rotate()
            #print("rotate!")
        